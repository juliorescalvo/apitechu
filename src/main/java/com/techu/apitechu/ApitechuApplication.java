package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		ApitechuApplication.productModels = ApitechuApplication.getTestData();

		SpringApplication.run(ApitechuApplication.class, args);
	}

	public static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> list = new ArrayList<ProductModel>();
		list.add(new ProductModel("1", "Producto 1", 345.34f));
		list.add(new ProductModel("2", "Producto 2", 10.50f));
		list.add(new ProductModel("3", "Producto 3", 9800.99f));

		return list;
	}

}
