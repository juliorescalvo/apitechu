package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

    private static final String API = "/apitechu/v1";

    @GetMapping(API + "/products")
    public ArrayList<ProductModel> getProducts() {
        return ApitechuApplication.productModels;
    }

    @GetMapping(API + "/products/{id-product}")
    public ProductModel getProductById(@PathVariable("id-product") String idProduct) {
        return ApitechuApplication.productModels.stream().filter(a -> a.getId().equals(idProduct)).findFirst().orElse(null);
    }

    @PostMapping(API + "/products")
    public ProductModel createProduct(@RequestBody ProductModel productModel) {
        ApitechuApplication.productModels.add(productModel);

        return productModel;
    }

    @PutMapping(API + "/products/{id-product}")
    public ProductModel updateProduct(@RequestBody ProductModel productModel, @PathVariable("id-product") String idProduct) {
        for (ProductModel productModel1 : ApitechuApplication.productModels) {
            if (productModel1.getId().equals(idProduct)) {
                productModel1.setId(productModel.getId());
                productModel1.setDesc(productModel.getDesc());
                productModel1.setPrice(productModel.getPrice());
            }
        }
        return productModel;
    }

    @PatchMapping(API + "/products/{id-product}")
    public ProductModel patchProduct(@RequestBody ProductModel productModel, @PathVariable("id-product") String idProduct) {
        for (ProductModel productModel1 : ApitechuApplication.productModels) {
            if (productModel1.getId().equals(idProduct)) {
                if (productModel.getDesc() != null) {
                    productModel1.setDesc(productModel.getDesc());
                }
                if (productModel.getPrice() > 0 && productModel1.getPrice() != productModel.getPrice()) {
                    productModel1.setPrice(productModel.getPrice());
                }
            }
        }
        return productModel;
    }

    @DeleteMapping(API + "/products/{id-product}")
    public void deleteProduct(@PathVariable("id-product") String idProduct) {
        ApitechuApplication.productModels.removeIf(l -> idProduct.equals(l.getId()));
    }

}
